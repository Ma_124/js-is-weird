# JS is weird
Scripts to compile every JS program into another equivalent JS program which only uses a small set of distinct characters.
Programs produced by [`main.js`](./main.js) use only `()[]+!`.
If you run `main.js` on itself, the resulting program is almost 6 million characters long (instead of the original 2&thinsp;500).

Inspired by <https://github.com/LowLevelJavaScript/JavaScript-Is-Weird/blob/master/index.js> (explained in <https://youtu.be/sRWE5tnaxlI>).
Their version uses `()[]+!- =>{}\/`.
