// inspired by https://github.com/LowLevelJavaScript/JavaScript-Is-Weird/blob/master/index.js (https://youtu.be/sRWE5tnaxlI)
// got rid of: ' ', '-', '=', '>', '{', '}', '\\', '/'
// still need:
// () Function invocation (grouping can be done using [expr][+[]])
// [] Lists + properties + toString
// +  toString + string concat + nums
// !  r + s from (true + false)

const zero = "+[]";
const one = "++[+[]][+[]]";

const evl = s => new Function("return " + s)();

const num = n => {
  if (n == 0) return zero;
  return "(" + Array.from({ length: n }, () => one).join(")+(") + ")";
}

const letters = {};

const str = s =>
  s.split("").map(x => {
    const c = letters[x];
    if (c === undefined) {
      throw `unknown letter '${x}' to construct '${s}'`;
    }
    return c;
  }).join("+");

const addLetters = code => {
  const word = evl(code);
  console.log(`${word} (${code})`);
  for (const i in word.split("")) {
    const oldCode = letters[word[i]];
    let newCode = `(${code})[${num(i)}]`;
    if (word.length === 1 && i === 0)
      newCode = code;

    if (oldCode === undefined || newCode.length < oldCode.length) {
      letters[word[i]] = newCode;
    }
  }
}

addLetters("![]+[]"); // false
addLetters("!![]+[]"); // true
/* alt:
  addLetters("[[]==[]][+[]]+[]");
  addLetters("[[]==[]+[]][+[]]+[]");
*/

addLetters("[][[]]+[]"); // undefined
addLetters(`[][${str("find")}]+[]`); // function find() { [native code] }
addLetters(`[]+([]+[])[${str('constructor')}]`); // function String() { [native code] }
for (let i = 2; i <= 36; i++) {
  addLetters(`(${num(i-1)})[${str('toString')}](${num(i)})`)
}
addLetters(`[][${str("constructor")}](${num(2)})+[]`) // [,,]
const funcConstr = `[][${str("at")}][${str("constructor")}]`
addLetters(`(${funcConstr}(${str("return escape")})()(${str(",")}))[${num(2)}]`); // %2C

console.log("====");
for (const l in letters) {
  if (l !== evl(letters[l]))
    throw `invalid construction for '${l}' (!= ${evl(letters[l])}): ${letters[l]}`;
  console.log(l);
}

console.log("====");
const str2 = s =>
  s.split("").map(x => {
    const c = letters[x];
    if (c === undefined) {
      return `([]+[])[${str("constructor")}][${str("fromCharCode")}](${num(x.charCodeAt(0))})`;
    }
    return c;
  }).join("+");

const compile = code => `${funcConstr}(${str2(code)})()`
const helloWorld = compile("console.log('Hello World');");
eval(helloWorld);

console.log(helloWorld.length);
console.log(Array.from(new Set(helloWorld.split(""))).sort());

//console.log("====");
//const fs = require("fs");
//const inp = fs.readFileSync(0, "utf-8");
//const out = compile(inp);
//console.log(out.length);
//console.log(Array.from(new Set(out.split(""))).sort());
//fs.writeFileSync("/tmp/out.js", out);

// https://stackoverflow.com/questions/35949554/invoking-a-function-without-parentheses
